//
//  ViewController.m
//  TKKeyboardManager
//
//  Created by Takaaki Kakinuma on 2017/01/24.
//  Copyright © 2017年 Takaaki Kakinuma. All rights reserved.
//

#import "ViewController.h"

#import "TKKeyboardManager.h"

@interface ViewController () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottonConstraint;
@property (nonatomic) TKKeyboardManager *keyboardManager;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.keyboardManager = [[TKKeyboardManager alloc] init];
    __weak typeof(self) weakSelf = self;
    [self.keyboardManager addKeyboardEventMoving:^(CGRect frame, BOOL isShow) {
        __strong typeof(self) strongSelf = weakSelf;
        if (isShow) {
            strongSelf.bottonConstraint.constant = frame.size.height - strongSelf.bottomLayoutGuide.length;
        } else {
            strongSelf.bottonConstraint.constant = 0;
        }
        [strongSelf.view layoutIfNeeded];
    }];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return true;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    return true;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
