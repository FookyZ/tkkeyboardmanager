//
//  main.m
//  TKKeyboardManager
//
//  Created by Takaaki Kakinuma on 2017/01/24.
//  Copyright © 2017年 Takaaki Kakinuma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
