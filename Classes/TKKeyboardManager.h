//
//  TKKeyboardManager.h
//  TKKeyboardManager
//
//  Created by Takaaki Kakinuma on 2017/01/24.
//  Copyright © 2017年 Takaaki Kakinuma. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef  void (^TKFrameBlock)(CGRect fram, BOOL isShow);

@interface TKKeyboardManager : NSObject

- (void)addKeyboardEventMoving:(TKFrameBlock)block;

@end

@interface NSNotification (Extension)

- (NSTimeInterval)duration;
- (UIViewAnimationCurve)curve;
- (CGRect)frameBegin;
- (CGRect)frameEnd;
- (BOOL)isLocalUser;

@end
