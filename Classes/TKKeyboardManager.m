//
//  TKKeyboardManager.m
//  TKKeyboardManager
//
//  Created by Takaaki Kakinuma on 2017/01/24.
//  Copyright © 2017年 Takaaki Kakinuma. All rights reserved.
//

#import "TKKeyboardManager.h"

@interface TKKeyboardManager ()

@property (nonatomic, copy) TKFrameBlock frameBlock;

@end

@implementation TKKeyboardManager

- (void)addKeyboardEventMoving:(TKFrameBlock)block {
    self.frameBlock = block;
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserverForName:UIKeyboardWillShowNotification
                        object:nil
                         queue:queue
                    usingBlock:^(NSNotification * _Nonnull note) {
                        [self showKeyboard:note];
                    }];
    [center addObserverForName:UIKeyboardDidShowNotification
                        object:nil
                         queue:queue
                    usingBlock:^(NSNotification * _Nonnull note) {
                        [self showKeyboard:note];
                    }];
    [center addObserverForName:UIKeyboardWillHideNotification
                        object:nil
                         queue:queue
                    usingBlock:^(NSNotification * _Nonnull note) {
                        [self hideKeyboard:note];
                    }];
    [center addObserverForName:UIKeyboardDidHideNotification
                        object:nil
                         queue:queue
                    usingBlock:^(NSNotification * _Nonnull note) {
                        [self hideKeyboard:note];
                    }];
}

- (void)showKeyboard:(NSNotification *)note {
    [UIView animateWithDuration:note.duration animations:^{
        if (self.frameBlock) {
            self.frameBlock(note.frameEnd, true);
        }
    }];
}

- (void)hideKeyboard:(NSNotification *)note {
    [UIView animateWithDuration:note.duration animations:^{
        if (self.frameBlock) {
            self.frameBlock(note.frameEnd, false);
        }
    }];
}

@end

@implementation NSNotification (Extension)

- (NSTimeInterval)duration {
    return [self.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
}

- (UIViewAnimationCurve)curve {
    return [self.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
}

- (CGRect)frameBegin {
    return [self.userInfo[UIKeyboardFrameBeginUserInfoKey] CGRectValue];
}

- (CGRect)frameEnd {
    return [self.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
}

- (BOOL)isLocalUser {
    return [self.userInfo[UIKeyboardIsLocalUserInfoKey] boolValue];
}

@end
